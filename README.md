# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### DOCUMENTATION
This project made use of React JS
1. Initialised a create-react-app
2. Added folder structure
3. Added scss npm package
4. Started adding layout, scss resets, variables, mixins & other styling neccessities
5. Created each Page, Section, and Component then styled them
6. Added routing using react-router-dom v6
7. Added Scroll animations using react-awesome-reveal npm package
8. Deployed with Netlify and Gitlab for agile development

This project was developed mobile first and was made responsive at Mobile (320px) and Desktop (1440px) breakpoints

### [VIEW ON FIGMA](https://www.figma.com/file/b5o5kQBpZbeaXK0oVIG0QM/Longevity?node-id=6%3A5642)

[Live Site](https://task-a.netlify.app)