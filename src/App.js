import { Layout } from "./components";
import { Contact, Home } from "./pages";
import { Footer } from "./sections";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
            
          <Route path="/contact" element={<Contact />} />
        </Routes>
        <Footer />
      </Layout>
    </Router>
  );
}

export default App;
