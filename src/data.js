export const cardData = [
  {
    icon: "ic:round-computer",
    title: "Lorem Ipsum",
    text: "Lorem ipsum dolor sit amet consectetur. Pellentesque aliquam id a adipiscing imperdiet. Amet porta pretium lectus ipsum. Purus commodo leo risus varius. Scelerisque gravida auctor vel aenean mi donec et. Proin nullam nunc est rutrum dignissim integer. Dui pretium sed pellentesque.",
  },
  {
    icon: "foundation:trees",
    title: "Lorem Ipsum",
    text: "Lorem ipsum dolor sit amet consectetur. Pellentesque aliquam id a adipiscing imperdiet. Amet porta pretium lectus ipsum. Purus commodo leo risus varius. Scelerisque gravida auctor vel aenean mi donec et. Proin nullam nunc est rutrum dignissim integer. Dui pretium sed pellentesque.",
  },
  {
    icon: "fontisto:direction-sign",
    title: "Lorem Ipsum",
    text: "Lorem ipsum dolor sit amet consectetur. Pellentesque aliquam id a adipiscing imperdiet. Amet porta pretium lectus ipsum. Purus commodo leo risus varius. Scelerisque gravida auctor vel aenean mi donec et. Proin nullam nunc est rutrum dignissim integer. Dui pretium sed pellentesque.",
  },
];

export const basicCardData = [
  {
    isAvailable: true,
    key: "key1",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key2",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key3",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key4",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key5",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key6",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key7",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key8",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key9",
    info: "Lorem ipsum dolor sit amet",
  },
];

export const premiumCardData = [
  {
    isAvailable: true,
    key: "key1",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key2",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key3",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key4",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key5",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key6",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key7",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key8",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key9",
    info: "Lorem ipsum dolor sit amet",
  },
];

export const standardCardData = [
  {
    isAvailable: true,
    key: "key1",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key2",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key3",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key4",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key5",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: true,
    key: "key6",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key7",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key8",
    info: "Lorem ipsum dolor sit amet",
  },
  {
    isAvailable: false,
    key: "key9",
    info: "Lorem ipsum dolor sit amet",
  },
];

export const faqData = [
  {
    question: "Lorem ipsum dolor sit amet consectetur. Erat ullamcorper morbi?",
    answer:
      "Lorem ipsum dolor sit amet consectetur. Habitasse elementum non etiam nisi commodo neque accumsan quisque. Et pellentesque diam tempus sollicitudin mauris lectus. Integer sed elementum nisi tortor et hac pellentesque viverra turpis. Imperdiet vitae cras et lectus elit erat in. Sit nulla adipiscing leo at quis vitae risus non.",
  },
  {
    question: "Lorem ipsum dolor sit amet consectetur. Erat ullamcorper morbi?",
    answer:
      "Lorem ipsum dolor sit amet consectetur. Habitasse elementum non etiam nisi commodo neque accumsan quisque. Et pellentesque diam tempus sollicitudin mauris lectus. Integer sed elementum nisi tortor et hac pellentesque viverra turpis. Imperdiet vitae cras et lectus elit erat in. Sit nulla adipiscing leo at quis vitae risus non.",
  },
  {
    question: "Lorem ipsum dolor sit amet consectetur. Erat ullamcorper morbi?",
    answer:
      "Lorem ipsum dolor sit amet consectetur. Habitasse elementum non etiam nisi commodo neque accumsan quisque. Et pellentesque diam tempus sollicitudin mauris lectus. Integer sed elementum nisi tortor et hac pellentesque viverra turpis. Imperdiet vitae cras et lectus elit erat in. Sit nulla adipiscing leo at quis vitae risus non.",
  },
];
