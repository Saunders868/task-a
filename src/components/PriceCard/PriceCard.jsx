import React from "react";
import Button from "../Button/Button";
import PriceCardFeature from "../PriceCardFeature/PriceCardFeature";

const PriceCard = ({ data, title, price }) => {
  return (
    <div className="price-card">
      <h2 className="price-card__title">{title}</h2>
      <div className="price-card__features">
        {data.map((feature) => (
          <PriceCardFeature
            isAvaialable={feature.isAvailable}
            info={feature.info}
            key={feature.key}
          />
        ))}
      </div>

      <h2 className="price-card__price">$ {price} US</h2>

      <Button small />
    </div>
  );
};

export default PriceCard;
