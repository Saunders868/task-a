import { Icon } from "@iconify/react";
import React from "react";

const Faq = ({ question,answer,toggle, i, selected}) => {
  return (
    <div className="faq__questions__question">
      <div onClick={() => toggle(i)}>
        <p>{question}</p>
        <Icon
          icon="akar-icons:chevron-down"
          width={"32"}
          rotate={selected === i ? 2 : 0}
        />
      </div>
      <p className={selected === i ? "answer open" : "answer"}>
        {answer}
      </p>
    </div>
  );
};

export default Faq;
