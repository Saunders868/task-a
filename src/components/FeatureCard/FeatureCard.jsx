import React from "react";
import { Icon } from "@iconify/react";

const FeatureCard = ({title, text, icon}) => {
  return (
    <div className="feature">
      <div className="feature__icon">
        <Icon icon={icon} width="64" />
      </div>
      <h3 className="feature__title">{title}</h3>
      <p className="feature__text">
        {text}
      </p>
    </div>
  );
};

export default FeatureCard;
