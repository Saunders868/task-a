export {default as Layout} from './Layout/Layout';
export {default as Button} from './Button/Button';
export {default as FeatureCard} from './FeatureCard/FeatureCard';
export {default as Slide} from './Slide/Slide';
export {default as PriceCard} from './PriceCard/PriceCard';
export {default as Faq} from './Faq/Faq';