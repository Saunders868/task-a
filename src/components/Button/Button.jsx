import React from "react";
import { Link } from 'react-router-dom'

const Button = ({ small, isSubmit, handleSubmit }) => {
  if (isSubmit) {
    return (
      <button onClick={handleSubmit} href="/" className={small ? "button__small" : "button"}>book a demo</button>
    )
  } else {
    return (
      <Link to="/contact" className={small ? "button__small" : "button"}>book a demo</Link>
    )
  }

};

export default Button;
