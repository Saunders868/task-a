import React from "react";
import { Icon } from "@iconify/react";

const PriceCardFeature = ({ isAvaialable, info }) => {
  return (
    <div className="price-card__feature">
      <Icon icon={isAvaialable ? "akar-icons:check" : "akar-icons:circle-x"} color={isAvaialable ? "#4ECB71" : "#F24E1E"} width="16" />
      <p>{info}</p>
    </div>
  );
};

export default PriceCardFeature;
