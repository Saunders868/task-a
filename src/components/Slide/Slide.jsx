import React from "react";

const Slide = ({ image, alt, testimonial, name, position, company }) => {
  return (
    <div className="slider">
      <div className="slider__image">
        <img src={image} alt={alt} />
      </div>
      <div className="slider__content">
        <h2>''</h2>
        <p>{testimonial}</p>
        <div className="slider__content__name">
          <h2>{name}</h2>
          <h3>
            {position} | {company}
          </h3>
        </div>
      </div>
    </div>
  );
};

export default Slide;
