import React, { useState } from "react";
import Faq from "../../components/Faq/Faq";
import { faqData } from "../../data";
import { Fade } from "react-awesome-reveal";

const FAQ = () => {
  const [selected, setSelected] = useState(null);

  const toggle = (i) => {
    if (selected === i) {
      return setSelected(null);
    }

    setSelected(i);
  };
  return (
    <section className="faq">
      <h2 className="faq__title">Want to know more?</h2>

      <div className="faq__questions">
        <Fade cascade damping={0.5}>
          {faqData.map((item, i) => (
            <Faq
              key={i}
              question={item.question}
              answer={item.answer}
              toggle={toggle}
              selected={selected}
              i={i}
            />
          ))}
        </Fade>
      </div>
    </section>
  );
};

export default FAQ;
