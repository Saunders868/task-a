export {default as Landing} from './Landing/Landing';
export {default as Features} from './Features/Features';
export {default as Testimonials} from './Testimonials/Testimonials';
export {default as Price} from './Price/Price';
export {default as FAQ} from './FAQ/FAQ';
export {default as Footer} from './Footer/Footer';