import { Icon } from "@iconify/react";
import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <a target={"_blank"} rel="noreferrer" href="https://gitlab.com/Saunders868/task-a">
        documentation
        <Icon icon="akar-icons:arrow-down-right" width="32" rotate={3} />
      </a>
      <p>&copy; daniel saunders</p>
    </footer>
  );
};

export default Footer;
