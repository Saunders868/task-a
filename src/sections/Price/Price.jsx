import React, { useState } from "react";
import PriceCard from "../../components/PriceCard/PriceCard";
import { basicCardData, standardCardData, premiumCardData } from "../../data";
import { Fade } from "react-awesome-reveal";

const Price = () => {
  const [isMonthly, setIsMonthly] = useState(true);

  function yearly() {
    setIsMonthly(false);
  }
  function monthly() {
    setIsMonthly(true);
  }
  return (
    <section className="price">
      <div className="price__tabs">
        <div className="price__tabs__wrapper">
          <span className={isMonthly ? "tab" : "tab right"} />
          <span className="price__tabs__wrapper__tab" onClick={monthly}>
            monthly
          </span>
          <span className="price__tabs__wrapper__tab" onClick={yearly}>
            yearly
          </span>
        </div>
      </div>

      {/* price cards */}
      <div className="price__cards">
        <Fade cascade damping={0.5}>
          <PriceCard
            data={basicCardData}
            title="Basic"
            price={isMonthly ? "20.00" : "15.00"}
          />
          <PriceCard
            data={standardCardData}
            title="Premium"
            price={isMonthly ? "60.00" : "55.00"}
          />
          <PriceCard
            data={premiumCardData}
            title="Standard"
            price={isMonthly ? "40.00" : "35.00"}
          />
        </Fade>
      </div>
    </section>
  );
};

export default Price;
