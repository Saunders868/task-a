import React from "react";
import stars from "../../assets/stars.png";
import homeImage from "../../assets/home-page.png";
import { Button } from "../../components";

const Landing = () => {
  return (
    <section className="landing">
      <div className="landing__info">
        <div className="landing__info__reviews">
          <img src={stars} alt="stars" />
          <p>reviewed by 143 people</p>
        </div>
        <h1 className="landing__info__heading">Extend your life expectancy by 20 years</h1>
        <p className="landing__info__content">
          We use AI to diagnose all the health risks that would affect you later
          on in life if not checked on.
        </p>
        <Button />
        <div className="landing__info__testimonial">
          <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. “</p>
          <div>
            <img className="test_img" src="https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" alt="person" />
            <div>
              <p>James Doe</p>
              <img src={stars} alt="stars" />
            </div>
          </div>
        </div>
      </div>
      <div className="landing__image">
        <img src={homeImage} alt="scientists" />
      </div>
    </section>
  );
};

export default Landing;
