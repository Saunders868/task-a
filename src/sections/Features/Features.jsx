import React from "react";
import { FeatureCard } from "../../components";
import { cardData } from "../../data";
import { Fade } from "react-awesome-reveal";

const Features = () => {
  return (
    <section className="features">
      <Fade cascade>
        {cardData.map((card, i) => (
          <FeatureCard
            key={i}
            title={card.title}
            text={card.text}
            icon={card.icon}
          />
        ))}
      </Fade>
    </section>
  );
};

export default Features;
