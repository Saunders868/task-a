import React from "react";
import "slick-carousel/slick/slick.css";
import Slider from "react-slick";
import testimonialFemale from "../../assets/testimonial.png";
import testimonialMale from "../../assets/testimonial-2.png";
import { Icon } from "@iconify/react";
import { Slide } from "../../components";
import { Fade } from "react-awesome-reveal";

function SampleNextArrow(props) {
  const { onClick } = props;
  return (
    <Icon
      icon="akar-icons:arrow-down-right"
      width="32"
      className="slider-arrow"
      onClick={onClick}
    />
  );
}

function SamplePrevArrow() {
  return <div />;
}

const testimonialData = [
  {
    image: testimonialFemale,
    alt: "woman with hands placed on knees",
    testimonial:
      "Lorem ipsum dolor sit amet consectetur. Mi tellus libero sed placerat amet. Adipiscing rhoncus faucibus tristique at. Risus auctor gravida auctor nisi.",
    name: "Jane Doe",
    position: "CEO",
    company: "InTime",
  },
  {
    image: testimonialMale,
    alt: "man with hands placed on face",
    testimonial:
      "Lorem ipsum dolor sit amet consectetur. Mi tellus libero sed placerat amet. Adipiscing rhoncus faucibus tristique at. Risus auctor gravida auctor nisi.",
    name: "James Doe",
    position: "Creative Director",
    company: "InTime",
  },
];

const Testimonials = () => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  return (
    <Fade direction="left" damping={1}>
      <div className="testimonials">
        <Slider {...settings}>
          {testimonialData.map((slide, i) => (
            <Slide
              key={i}
              image={slide.image}
              alt={slide.alt}
              testimonial={slide.testimonial}
              name={slide.name}
              position={slide.position}
              company={slide.company}
            />
          ))}
        </Slider>
      </div>
    </Fade>
  );
};

export default Testimonials;
