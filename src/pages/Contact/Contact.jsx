import React, { useState } from "react";
import mail from "../../assets/conatct-page.png";
import { Button } from "../../components";

const Contact = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    number: "",
    confirm: false,
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    localStorage.setItem("name: ", formData.name);
    localStorage.setItem("email: ", formData.email);
    localStorage.setItem("number: ", formData.number);
    setFormData({ confirm: true });

    setTimeout(() => setFormData({ confirm: false }), 5000);
  };
  return (
    <main className="contact">
      <div className="contact__image">
        <img src={mail} alt="man sending mail" />
      </div>
      <form className="form" onSubmit={handleSubmit}>
        <div className="form__group">
          <label>Name :</label>
          <input
            className="form__group__input"
            type="text"
            onChange={(e) => setFormData({ ...formData, name: e.target.value })}
            required
            placeholder="name..."
          />
        </div>
        <div className="form__group">
          <label>Email :</label>
          <input
            className="form__group__input"
            type="email"
            onChange={(e) => setFormData({ ...formData, email: e.target.value })}
            required
            placeholder="email..."
          />
        </div>
        <div className="form__group">
          <label>Phone Number :</label>
          <input
            className="form__group__input"
            type="text"
            onChange={(e) => setFormData({ ...formData, number: e.target.value })}
            placeholder="phone number..."
          />
        </div>

        <Button isSubmit handleSubmit={handleSubmit} />
      </form>

      <div className={formData.confirm ? "contact__confirm success" : "contact__confirm"}>
          <p>your demo has been booked!</p>
        </div>
    </main>
  );
};

export default Contact;
