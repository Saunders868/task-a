import React from "react";
import { FAQ, Features, Landing, Price, Testimonials } from "../../sections";

const Home = () => {
  return (
    <main>
      <Landing />
      <Features />
      <Testimonials />
      <Price />
      <FAQ />
    </main>
  );
};

export default Home;
